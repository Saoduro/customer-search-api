var email = context.getVariable("request.queryparam.email");
var emailRegex = new RegExp(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/);
      context.setVariable("isValidRequest", true);
if(!emailRegex.test(email)) {
       context.setVariable("isValidRequest", false);
         context.setVariable("errorMessage", "Please provide a valid email.");
        context.setVariable("paramName", "email");
}